package Controller;

import Model.EducationalSystem.EducationalAssistant;
import Model.EducationalSystem.Student;
import Model.Repo;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

import java.io.IOException;

import static Controller.EducationalAssistantController.genEducationalController;
import static Controller.StudentController.genStudentController;

public class LoginController {
    @FXML
    private TextField usernameTextField;
    @FXML
    private TextField passwordTextField;
    @FXML
    private Button loginButton;


    public LoginController() {
    }

    @FXML
    private void initialize() {
        loginButton.setOnAction(event ->
                Repo.getRepo().getUsers().forEach(user -> {
                    try {
                        if (user.getUsername().equals(usernameTextField.getText()) &&
                                user.isPassword(passwordTextField.getText()))
                            if (user instanceof Student)
                                genStudentController(event, user);
                            else if (user instanceof EducationalAssistant)
                                genEducationalController(event, user);

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                })); // Yo ho ho! lisp syntax!!
        usernameTextField.setOnAction(event -> loginButton.fire());
        passwordTextField.setOnAction(event -> loginButton.fire());
    }
}
