package Controller;

import Model.EducationalSystem.Student;
import Model.UserManagment.User;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;

public class StudentController {
    private Student student;

    static private StudentController instance;

    @NotNull
    static public StudentController genStudentController(@NotNull ActionEvent event, User user) throws IOException {
        return new StudentController(event, user);
    }

    private StudentController(@NotNull ActionEvent event, User user) throws IOException {
        instance = this;
        this.student = (Student) user;
        Parent parent= FXMLLoader.load(getClass().getResource("student.fxml"));
        Scene scene= new Scene(parent);
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.show();
    }



    public static StudentController getInstance() {
        return instance;
    }

    public Student getStudent() {
        return student;
    }
}
