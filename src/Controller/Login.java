package Controller;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import sample.Main;

import java.io.IOException;

public class Login {

    public Login() throws IOException{
        Parent root = FXMLLoader.load(getClass().getResource("login.fxml"));
        Stage stage = Main.getStage();
        stage.setTitle("EMS");
        Scene scene = new Scene(root, 800, 500);
        stage.setScene(scene);
        stage.show();
    }
}
