package Controller;

import Model.EducationalSystem.EducationalAssistant;
import Model.UserManagment.User;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;

public class EducationalAssistantController {
    private EducationalAssistant educationalAssistant;

    static private EducationalAssistantController instance;

    static public EducationalAssistantController genEducationalController(@NotNull ActionEvent event, User user) throws IOException {
        return new EducationalAssistantController(event, user);
    }

    private EducationalAssistantController(@NotNull ActionEvent event, User user) throws IOException {
        instance = this;
        this.educationalAssistant = (EducationalAssistant) user;
        Parent parent = FXMLLoader.load(getClass().getResource("educational_assistant.fxml"));
        Scene scene = new Scene(parent);
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.show();
    }
}
