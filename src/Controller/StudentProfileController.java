package Controller;

import Model.EducationalSystem.Student;
import Model.Messeging.Message;
import Model.Messeging.Party;
import Model.Repo;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import org.jetbrains.annotations.NotNull;
import sample.Main;

import java.io.IOException;
import java.util.ArrayList;

public class StudentProfileController {
    @FXML
    private TableView profileTable;
    @FXML
    private TableColumn<StudentProfileTableViewItem, String> valueColumn;
    @FXML
    private TableColumn<StudentProfileTableViewItem, String> keyColumn;
    @NotNull
    @FXML
    private ObservableList<StudentProfileTableViewItem> profileData = FXCollections.observableArrayList();

    @FXML
    private MenuItem logoutMenuItem;
    @FXML
    private ListView<String> messageListView;
    @FXML
    private ObservableList<String> messageData = FXCollections.observableArrayList();


    @FXML
    private TextField titleTextField;
    @FXML
    private TextArea contentTextArea;
    @FXML
    private ComboBox<String> recipientComboBox;
    @FXML
    private Button sendMessageBtn;


    public StudentProfileController() {
        Student s = StudentController.getInstance().getStudent();
        profileData.addAll(StudentProfileTableViewItem.genItems(s));

        ArrayList<String> items = new ArrayList<>();
        s.getInbox().getReceived().forEach(message -> {
            String content = "Sender: " + message.getRecipientInbox().getParty().getUsername() + "\n";
            content += "Title: " + message.getTitle();
            content += "Date sent: " + message.getSentDateTime().getYear() + "/"+
                    message.getSentDateTime().getMonth()+"/"+
                    message.getSentDateTime().getDayOfMonth() + "\n";
            content += "Content:\n" + message.getContent();
            items.add(content);
        });
        messageData.addAll(items);
    }

    @FXML
    private void initialize() {
        Student s = StudentController.getInstance().getStudent();
        // show info
        keyColumn.setCellValueFactory(cellData -> cellData.getValue().keyProperty());
        valueColumn.setCellValueFactory(cellData -> cellData.getValue().valueProperty());
        profileTable.setItems(profileData);

        // logout button
        logoutMenuItem.setOnAction(event -> {
            try {
                new Login();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        // show messages
        messageListView.getItems().addAll(messageData);

        // populate combobox
        Repo.getRepo().getParties().forEach(party -> {
            if (party.getUsername().equals(s.getUsername()) == false)
                recipientComboBox.getItems().add(party.getUsername());
        });

        // set send Message signal
        sendMessageBtn.setOnAction(event -> {
            Message.sendMessage(contentTextArea.getText(),
                    titleTextField.getText(),
                    s,
                    Party.getPartyByUsername(recipientComboBox.getValue())
            );
        });

    }
}
