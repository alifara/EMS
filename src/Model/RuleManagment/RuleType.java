package Model.RuleManagment;

public enum RuleType {
    CouseStudentCountLimit,
    StartAndFinish4Course,
    AddAndRemoveInterval,
    ExamDate,
    MaxGraderCount;
}
