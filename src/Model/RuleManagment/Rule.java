package Model.RuleManagment;

import Model.Repo;
import Model.SystemChart.Department;

abstract public class Rule {
    private RuleType ruleType;
    private Department department;

    public Rule(RuleType ruleType, Department department) {
        this.ruleType = ruleType;
        this.department = department;
        department.getRules().add(this);

        Repo.getRepo().getRules().add(this);
    }

    public RuleType getRuleType() {
        return ruleType;
    }

    public void setRuleType(RuleType ruleType) {
        this.ruleType = ruleType;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }
}
