package Model.RuleManagment.Rules;

import Model.RuleManagment.Rule;
import Model.RuleManagment.RuleType;
import Model.SystemChart.Department;

public class RuleAddAndRemoveInterval extends Rule {
    public RuleAddAndRemoveInterval(Department department) {
        super(RuleType.AddAndRemoveInterval, department);
    }

}
