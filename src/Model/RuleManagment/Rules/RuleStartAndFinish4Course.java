package Model.RuleManagment.Rules;

import Model.EducationalSystem.Course;
import Model.RuleManagment.Rule;
import Model.RuleManagment.RuleType;
import Model.SystemChart.Department;

public class RuleStartAndFinish4Course extends Rule {
    private Course course;

    public RuleStartAndFinish4Course(Department department, Course course) {
        super(RuleType.StartAndFinish4Course, department);
        this.course = course;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }
}
