package Model.RuleManagment.Rules;

import Model.EducationalSystem.Course;
import Model.RuleManagment.Rule;
import Model.RuleManagment.RuleType;
import Model.SystemChart.Department;

public class RuleMaxGraderCount extends Rule {
    private int maxGraderCount;
    private Course course;

    public RuleMaxGraderCount(Department department, int maxGraderCount, Course course) {
        super(RuleType.MaxGraderCount, department);
        this.maxGraderCount = maxGraderCount;
        this.course = course;
    }

    public int getMaxGraderCount() {
        return maxGraderCount;
    }

    public void setMaxGraderCount(int maxGraderCount) {
        this.maxGraderCount = maxGraderCount;
    }
}
