package Model.RuleManagment.Rules;

import Model.EducationalSystem.Course;
import Model.RuleManagment.Rule;
import Model.RuleManagment.RuleType;
import Model.SystemChart.Department;

public class RuleCourseStudentCountLimit extends Rule {
    private int maxCount;
    private Course course;

    public RuleCourseStudentCountLimit(Department department, int maxCount, Course course) {
        super(RuleType.CouseStudentCountLimit, department);
        this.maxCount = maxCount;
        this.course = course;
    }


}
