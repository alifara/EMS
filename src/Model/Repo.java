package Model;


import Model.EducationalSystem.*;
import Model.Messeging.Inbox;
import Model.Messeging.Message;
import Model.Messeging.Party;
import Model.RuleManagment.Rule;
import Model.SystemChart.Department;
import Model.SystemChart.Staff;
import Model.UserManagment.User;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

public class Repo {
    private ArrayList<Student> students = new ArrayList<>();
    private ArrayList<Message> messages = new ArrayList<>();
    private ArrayList<Inbox> inboxes = new ArrayList<>();
    private ArrayList<Professor> professors = new ArrayList<>();
    private ArrayList<Homework> homeworks = new ArrayList<>();
    private ArrayList<Grade> grades = new ArrayList<>();
    private ArrayList<Course> courses= new ArrayList<>();
    private ArrayList<Semester> semesters= new ArrayList<>();
    private ArrayList<EducationalAssistant> educationalAssistants = new ArrayList<>();
    private ArrayList<AdviserProfessor> adviserProfessors = new ArrayList<>();
    private ArrayList<Department> departments = new ArrayList<>();
    private ArrayList<Rule> rules = new ArrayList<>();


    @NotNull
    private static Repo ourInstance = new Repo();

    @NotNull
    public static Repo getRepo() {
        return ourInstance;
    }

    private Repo() {

    }

    @NotNull
    public ArrayList<Staff> getStaffs() {
        ArrayList<Staff> staffs = new ArrayList<>();

        staffs.addAll(professors);
        staffs.addAll(adviserProfessors);
        staffs.addAll(educationalAssistants);

        return staffs;
    }

    @NotNull
    public ArrayList<User> getUsers() {
        ArrayList<User> users = new ArrayList<>();

        users.addAll(students);
        users.addAll(professors);
        users.addAll(educationalAssistants);
        users.addAll(adviserProfessors);

        return users;
    }


    @NotNull
    public ArrayList<Party> getParties() {
        ArrayList<Party> parties = new ArrayList<>();

        parties.addAll(students);
        parties.addAll(professors);
        parties.addAll(educationalAssistants);
        parties.addAll(adviserProfessors);

        return parties;
    }

    public ArrayList<AdviserProfessor> getAdviserProfessors() {
        return adviserProfessors;
    }

    public void setAdviserProfessors(ArrayList<AdviserProfessor> adviserProfessors) {
        this.adviserProfessors = adviserProfessors;
    }

    public ArrayList<Professor> getProfessors() {
        return professors;
    }

    public void setProfessors(ArrayList<Professor> professors) {
        this.professors = professors;
    }

    public ArrayList<Homework> getHomeworks() {
        return homeworks;
    }

    public void setHomeworks(ArrayList<Homework> homeworks) {
        this.homeworks = homeworks;
    }

    public ArrayList<Grade> getGrades() {
        return grades;
    }

    public void setGrades(ArrayList<Grade> grades) {
        this.grades = grades;
    }

    public ArrayList<Course> getCourses() {
        return courses;
    }

    public void setCourses(ArrayList<Course> courses) {
        this.courses = courses;
    }

    public ArrayList<Message> getMessages() {
        return messages;
    }

    public void setMessages(ArrayList<Message> messages) {
        this.messages = messages;
    }

    public ArrayList<Inbox> getInboxes() {
        return inboxes;
    }

    public void setInboxes(ArrayList<Inbox> inboxes) {
        this.inboxes = inboxes;
    }

    public ArrayList<Student> getStudents() {
        return students;
    }

    public void setStudents(ArrayList<Student> students) {
        this.students = students;
    }

    public ArrayList<Semester> getSemesters() {
        return semesters;
    }

    public void setSemesters(ArrayList<Semester> semesters) {
        this.semesters = semesters;
    }

    public ArrayList<EducationalAssistant> getEducationalAssistants() {
        return educationalAssistants;
    }

    public void setEducationalAssistants(ArrayList<EducationalAssistant> educationalAssistants) {
        this.educationalAssistants = educationalAssistants;
    }

    public ArrayList<Department> getDepartments() {
        return departments;
    }

    public void setDepartments(ArrayList<Department> departments) {
        this.departments = departments;
    }

    public ArrayList<Rule> getRules() {
        return rules;
    }

    public void setRules(ArrayList<Rule> rules) {
        this.rules = rules;
    }
}
