package Model.UserManagment;

import org.jetbrains.annotations.NotNull;

import java.security.MessageDigest;
import java.util.Calendar;

abstract public class User {
    protected String firstName;
    protected String lastName;
    protected String username;
    protected String password;
    protected Calendar birthdate;
    protected String nationalID;
    @NotNull
    protected Gender gender = Gender.MALE;

    public enum Gender {
        MALE("MALE"),
        FEMALE("FEMALE");

        private String name;

        Gender(String name){ this.name = name; }

        public String getName() {
            return name;
        }

        public void setName(@NotNull String name) {
            if (name.equalsIgnoreCase("MALE") ||
                    name.equalsIgnoreCase("FEMALE"))
                this.name = name.toUpperCase();
        }
    }

    public User(String firstName, String lastName, String username, String password, Calendar birthdate, String nationalID, @NotNull String genderStr) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.username = username;
        this.password = get_SHA_512_SecurePassword(password, "0");
        this.birthdate = birthdate;
        this.nationalID = nationalID;
        this.gender.setName(genderStr);

    }

    private static String get_SHA_512_SecurePassword(String passwordToHash, String   salt){
        String generatedPassword = null;
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-512");
            md.update(salt.getBytes("UTF-8"));
            byte[] bytes = md.digest(passwordToHash.getBytes("UTF-8"));
            StringBuilder sb = new StringBuilder();
            for(int i=0; i< bytes.length ;i++){
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            generatedPassword = sb.toString();
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return generatedPassword;
    }

    public boolean isPassword(String password) {
        return this.password.equals(
                get_SHA_512_SecurePassword(password, "0"));
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Calendar getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(Calendar birthdate) {
        this.birthdate = birthdate;
    }

    public String getNationalID() {
        return nationalID;
    }

    public void setNationalID(String nationalID) {
        this.nationalID = nationalID;
    }
}
