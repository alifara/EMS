package Model.SystemChart;

import Model.EducationalSystem.Course;
import Model.EducationalSystem.EducationalAssistant;
import Model.EducationalSystem.Student;
import Model.Repo;
import Model.RuleManagment.Rule;
import Model.RuleManagment.RuleType;

import java.util.ArrayList;

public class Department {
    private ArrayList<Student> students;
    private ArrayList<FacultyMember> facultyMembers;
    private EducationalAssistant educationalAssistant;
    private ArrayList<Course> courses;
    private ArrayList<Rule> rules;

    public Department() {
        Repo.getRepo().getDepartments().add(this);
    }

    public boolean editCourseIsActive() {
        for (Rule rule : rules)
            if (rule.getRuleType() == RuleType.AddAndRemoveInterval)
                return true;
        return false;
    }

    public void addCourse(Course course) {
        this.getCourses().add(course);
    }

    public void removeCourse(Course course) {
          for (Course c : courses)
              if (c == course)
                  courses.remove(course);
    }

    public ArrayList<Student> getStudents() {
        return students;
    }

    public void setStudents(ArrayList<Student> students) {
        this.students = students;
    }

    public ArrayList<FacultyMember> getFacultyMembers() {
        return facultyMembers;
    }

    public void setFacultyMembers(ArrayList<FacultyMember> facultyMembers) {
        this.facultyMembers = facultyMembers;
    }

    public EducationalAssistant getEducationalAssistant() {
        return educationalAssistant;
    }

    public void setEducationalAssistant(EducationalAssistant educationalAssistant) {
        this.educationalAssistant = educationalAssistant;
    }

    public ArrayList<Course> getCourses() {
        return courses;
    }

    public void setCourses(ArrayList<Course> courses) {
        this.courses = courses;
    }

    public ArrayList<Rule> getRules() {
        return rules;
    }

    public void setRules(ArrayList<Rule> rules) {
        this.rules = rules;
    }
}
