package Model.EducationalSystem;

import Model.Repo;

import java.util.ArrayList;

public class Grade {
    private Double score;
    private Student student;
    private Course course;
    private ArrayList<Homework> homeworks = new ArrayList<>();

    public Grade(Double score, Student student, Course course) {
        this.score = score;
        this.student = student;
        this.course = course;

        Repo.getRepo().getGrades().add(this);
    }

    public Double getScore() {
        return score;
    }

    public void setScore(Double score) {
        this.score = score;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public ArrayList<Homework> getHomeworks() {
        return homeworks;
    }

    public void setHomeworks(ArrayList<Homework> homework) {
        this.homeworks = homework;
    }
}
