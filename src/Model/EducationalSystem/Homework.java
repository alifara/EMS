package Model.EducationalSystem;

import Model.Repo;

import java.util.Calendar;

public class Homework  {
    private Calendar assignDate;
    private Calendar deadline;
    private String content;
private Double score = 0.0;

    public Homework(Homework homework) {
        this.assignDate = homework.assignDate;
        this.deadline = homework.deadline;
        this.content = homework.content;

        Repo.getRepo().getHomeworks().add(this);
    }

    public Homework(Calendar deadline, String content) {
        assignDate = Calendar.getInstance();
        this.deadline = deadline;
        this.content = content;

        Repo.getRepo().getHomeworks().add(this);
    }

    public void setDeadline(Calendar deadline) {
        this.deadline = deadline;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Calendar getAssignDate() {

        return assignDate;
    }

    public Calendar getDeadline() {
        return deadline;
    }

    public String getContent() {
        return content;
    }

    public Double getScore() {
        return score;
    }

    public void setScore(Double score) {
        this.score = score;
    }
}
