package Model.EducationalSystem.Exceptions;

public class ExceptionCourseFull extends ExceptionCourse{
    public String message = "Attempt at adding student to course which is full.";
}
