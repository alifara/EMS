package Model.EducationalSystem;

import Model.EducationalSystem.Exceptions.ExceptionCourseFull;
import Model.Repo;

import java.util.ArrayList;

public class Course {
    private String name;
    private Professor instructor;
    private Semester semester;

    private ArrayList<Student> graders;
    private ArrayList<Grade> grades;
    private ArrayList<Homework> homework;

    private int maxStudents;

    public Course(String name, Semester semester, int maxStudents) {
        this.name = name;
        this.semester = semester;
        this.maxStudents = maxStudents;

        Repo.getRepo().getCourses().add(this);
    }

    public Course(String name, Professor instructor, Semester semester, int maxStudents) {
        this.name = name;
        this.instructor = instructor;
        this.semester = semester;
        this.maxStudents = maxStudents;

        Repo.getRepo().getCourses().add(this);
    }

    public void removeInstructor() {
        if (instructor != null)
            instructor.getCourses().remove(this);
        instructor = null;
    }

    public void assignInstructor(Professor instructor) {
        this.removeInstructor();
        this.setInstructor(instructor);
        instructor.getCourses().add(this);
    }

    // release grade object, break chain student-x-grade-x-course
    public void removeStudent(Student student) {
        Grade targetGrade = null;

        for (Grade grade : grades) {
            if (grade.getStudent().getUsername().equals(student.getUsername()))
                targetGrade = grade;
        }

        if (targetGrade != null) {
            this.grades.remove(targetGrade);
            student.getGrades().remove(targetGrade);
            // cleanup grade object
            targetGrade.setStudent(null);
            targetGrade.setScore(null);
            targetGrade.setCourse(null);
            targetGrade.setHomeworks(null);
        }
    }

    // create grade object, chain student-grade-course
    public void addStudent(Student student) throws ExceptionCourseFull {
        if (this.maxStudents == this.grades.size())
            throw new ExceptionCourseFull();

        boolean isDouble = false;
        for (Grade grade : grades) {
            if (grade.getStudent().getUsername().equals(student.getUsername()))
                isDouble = false;
        }
        if (!isDouble) {
            Grade grade = new Grade(0.0, student, this);
            this.grades.add(grade);
            student.getGrades().add(grade);
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Professor getInstructor() {
        return instructor;
    }

    public void setInstructor(Professor instructor) {
        this.instructor = instructor;
    }

    public Semester getSemester() {
        return semester;
    }

    public void setSemester(Semester semester) {
        this.semester = semester;
    }

    public ArrayList<Student> getGraders() {
        return graders;
    }

    public void setGraders(ArrayList<Student> graders) {
        this.graders = graders;
    }

    public ArrayList<Grade> getGrades() {
        return grades;
    }

    public void setGrades(ArrayList<Grade> grades) {
        this.grades = grades;
    }

    public int getMaxStudents() {
        return maxStudents;
    }

    public void setMaxStudents(int maxStudents) {
        this.maxStudents = maxStudents;
    }

    public ArrayList<Homework> getHomework() {
        return homework;
    }

    public void setHomework(ArrayList<Homework> homework) {
        this.homework = homework;
    }
}
