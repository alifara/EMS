package Model.EducationalSystem;

import Model.Repo;
import com.sun.org.apache.regexp.internal.RE;

import java.util.Calendar;

public class Semester {
    private Calendar start, end;

    public Semester(Calendar start, Calendar end) {
        this.start = start;
        this.end = end;

        Repo.getRepo().getSemesters().add(this);
    }

    public Calendar getStart() {
        return start;
    }

    public void setStart(Calendar start) {
        this.start = start;
    }

    public Calendar getEnd() {
        return end;
    }

    public void setEnd(Calendar end) {
        this.end = end;
    }
}
