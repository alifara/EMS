package Model.EducationalSystem;

import Model.Messeging.Inbox;
import Model.Messeging.Party;
import Model.Repo;
import Model.UserManagment.User;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Calendar;

public class Student extends User implements Party{
    private String email;
    private String phoneNumber;
    private Calendar entryDate;
    private String residentialAddress;
    private ArrayList<Major> majors = new ArrayList<>();
    private ArrayList<Grade> grades = new ArrayList<>();
    private ArrayList<Course> courseGraderies = new ArrayList<>();
    private AlmaMater almaMater = AlmaMater.BSC;
    private Inbox inbox;

    public Student(String firstName, String lastName, String username, String password, Calendar birthdate, String nationalID, @NotNull String genderStr, String email, String phoneNumber, Calendar entryDate, String residentialAddress, String almaMaterStr) {
        super(firstName, lastName, username, password, birthdate, nationalID, genderStr);
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.entryDate = entryDate;
        this.residentialAddress = residentialAddress;
        this.almaMater.setName(almaMaterStr);
        this.inbox = new Inbox(this);

        Repo.getRepo().getStudents().add(this);
    }

    static public Student findStudentByID(String nationalID) {
        Repo repo = Repo.getRepo();
        for (Student student : repo.getStudents())
            if (student.getNationalID().equals(nationalID))
                return student;
        return null;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Calendar getEntryDate() {
        return entryDate;
    }

    public void setEntryDate(Calendar entryDate) {
        this.entryDate = entryDate;
    }

    public String getResidentialAddress() {
        return residentialAddress;
    }

    public void setResidentialAddress(String residentialAddress) {
        this.residentialAddress = residentialAddress;
    }

    public ArrayList<Course> getCourses() {
        ArrayList<Course> courses = new ArrayList<>();
        this.grades.forEach(grade -> {
            courses.add(grade.getCourse());
        });
        return courses;
    }

    public ArrayList<Grade> getGrades() {
        return grades;
    }

    public void setGrades(ArrayList<Grade> grades) {
        this.grades = grades;
    }

    public ArrayList<Course> getCourseGraderies() {
        return courseGraderies;
    }

    public void setCourseGraderies(ArrayList<Course> courseGraderies) {
        this.courseGraderies = courseGraderies;
    }

    public AlmaMater getAlmaMater() {
        return almaMater;
    }

    public void setAlmaMater(AlmaMater almaMater) {
        this.almaMater = almaMater;
    }

    @Override
    public Inbox getInbox() {
        return inbox;
    }

    public void setInbox(Inbox inbox) {
        this.inbox = inbox;
    }

    public ArrayList<Major> getMajors() {
        return majors;
    }

    public void setMajors(ArrayList<Major> majors) {
        this.majors = majors;
    }
}
