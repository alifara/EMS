package Model.EducationalSystem;

import Model.SystemChart.Department;

public class Major {
    private String name;
    private Department department;


    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }
}
