package Model.EducationalSystem;

import Model.Repo;
import Model.SystemChart.Staff;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Calendar;

public class Professor extends Staff  {
    private ArrayList<Course> courses;

    public Professor(String firstName, String lastName, String username, String password, Calendar birthdate, String nationalID, @NotNull String genderStr, ArrayList<Course> courses) {
        super(firstName, lastName, username, password, birthdate, nationalID, genderStr);
        this.courses = courses;

        Repo.getRepo().getProfessors().add(this);
    }

    public void assignGrader2Course(Student grader, Course course) {
        course.getGraders().add(grader);
        grader.getCourseGraderies().add(course);
    }

    public void assignHomework2Course(Homework homework, Course course) {
        course.getGrades().forEach(grade -> {
            grade.getHomeworks().add(new Homework(homework));
        });
    }

    public void assignScore2Homework(Student student, Homework homework, Double score) {
        Repo.getRepo().getGrades().forEach(grade -> {
            Homework targetHw = null;
            for (Homework homework1 : grade.getHomeworks())
                if (homework1.getContent().equals(homework.getContent()))
                    targetHw = homework1;
            if (grade.getStudent().getUsername().equals(student.getUsername()) &&
                    targetHw != null)
                targetHw.getScore();
        });
    }

    public void assignScore2StudentCourse(Student student, Course course, Double score){
        Repo.getRepo().getGrades().forEach(grade -> {
            if (grade.getStudent().getUsername().equals(student.getUsername()) &&
                    grade.getCourse() == course)
                grade.setScore(score);
        });
    }

    public ArrayList<Course> getCourses() {
        return courses;
    }

    public void setCourses(ArrayList<Course> courses) {
        this.courses = courses;
    }
}
